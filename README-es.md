# Topología de red Etapa Estelar de Villa Club en Packet Tracer

Diseñamos esta configuración de red para una urbanización, dadas algunas limitaciones que se nos plantearon para el desarrollo del proyecto. El tema principal es acerca de un adecuado direccionamiento de red.
Es un proyecto interesante, porque nos plantea el hecho de configurar la red de una urbanización, dando libre elección. Presentando ciertos detalles al momento del desarrollo y compartiéndolo con otros usuarios para su guía a futuro.

## Red Diseñada

![a](../master/network_topology.jpeg)
 
**Urbanización:** Villa Club Etapa Estelar (Google Maps)

## Planteamiento

En el Proyecto se nos propuso elegir una etapa de una urbanización, siguiendo ciertas indicaciones. 
“Cada grupo deberá escoger una urbanización (mediante google maps) y una dirección de red de clase B. Luego en base al número de casas y manzanas realizar un adecuado direccionamiento de red que evite que el broadcast de una manzana afecte a otra (subredes y vlans)”.
Tomando en cuenta esto diseñamos la red de la urbanización con 4 computadores conectados a un mismo dispositivo de red(switch), cubriendo una manzana de la urbanización. 
Dado que son 16 manzanas las que posee la Etapa Estelar, se necesita de 64 pc, conectados a los dispositivos de red correspondientes:
 
* Mz 1(VLAN 2): 134.18.1.97
* Mz 2(VLAN 4): 134.18.0.161
* Mz 3(VLAN 6): 134.18.0.193
* Mz 4(VLAN 8): 134.18.1.33
* Mz 5(VLAN 10):134.18.0.225
* Mz 6(VLAN 12):134.18.1.129
* Mz 7(VLAN 14): 134.18.1.1
* Mz 8(VLAN 16):134.18.1.225
* Mz 9(VLAN 18): 134.18.1.65
* Mz 10(VLAN 20):134.18.1.193
* Mz 11(VLAN 22): 134.18.1.161
* Mz 12(VLAN 24): 134.18.0.129
* Mz 13(VLAN 26): 134.18.0.1
* Mz 14(VLAN 28): 134.18.0.65
* Mz 15(VLAN 30): 134.18.2.17
* Mz 16(VLAN 32): 134.18.2.1
 
Como se nos pidió, todos los dispositivos de red se deben encontrar interconectados y tener una salida a la internet mediante una única conexión. 

## Limitaciones
Para facilidad del trabajo desarrollamos la simulación de la topología en el Cisco Packet Tracer para diseñar de manera óptima y segura la red, sabiendo cuantos dispositivos se iban a emplear, siguiendo los puntos que nos especificaron.
Se nos planteó elegir un método para realizar el direccionamiento de la red que hayamos seleccionado, nosotros elegimos el método de router on a stick; el cual es una de los métodos más comunes para hacer el enrutamiento inter-VLAN. Consiste en crear sub interfaces en una sola interface física de un router; tenemos con esta configuración la posibilidad de utilizar una sola interface para enrutar los paquetes de varias VLAN que viajan a través del puerto trunk de un switch conectado a esa interface.

## Contribuyentes
- Juan Sotomayor
- Hugo Gavilánez
- Roberth Franco
- Ricardo Ramírez

## Como Ejecutar el Proyecto
Instalar [Cisco Packet Tracer](https://www.netacad.com/courses/packet-tracer) y descargar el proyecto [Villa Club Etapa Estelar.pkt](https://github.com/LonelyWolf54i/Network-Simulation/blob/master/Villa%20Club%20Etapa%20Estelar.pkt), ejecutarlo y ver el funcionamiento adecuado.
