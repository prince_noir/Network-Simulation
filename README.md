# Star Club network stage topology at Packet Tracer

We designed this network configuration for an urbanization, given some limitations that were presented to us for the development of the project. The main topic is about proper network addressing.
It is an interesting project, because it raises the fact of configuring the network of an urbanization, giving free choice. Presenting certain details at the time of development and sharing it with other users for future guidance.

## Designed Network

![a](../master/network_topology.jpeg)
 

**Urbanization:** Villa Club Etapa Estelar (Google Maps)

## Approach

In the Project we were proposed to choose a stage of an urbanization, following certain indications.
“Each group must choose an urbanization (using google maps) and a class B network address. Then, based on the number of houses and apples, make an adequate network address that prevents the broadcast of one apple from affecting another (subnets and vlans) ”
Taking this into account, we designed the urbanization network with 4 computers connected to the same network device(switch), covering one block of the urbanization.
Since there are 16 blocks that the Star Stage has, it takes 64 pcs, connected to the corresponding network devices:
 
* Mz 1(VLAN 2): 134.18.1.97
* Mz 2(VLAN 4): 134.18.0.161
* Mz 3(VLAN 6): 134.18.0.193
* Mz 4(VLAN 8): 134.18.1.33
* Mz 5(VLAN 10):134.18.0.225
* Mz 6(VLAN 12):134.18.1.129
* Mz 7(VLAN 14): 134.18.1.1
* Mz 8(VLAN 16):134.18.1.225
* Mz 9(VLAN 18): 134.18.1.65
* Mz 10(VLAN 20):134.18.1.193
* Mz 11(VLAN 22): 134.18.1.161
* Mz 12(VLAN 24): 134.18.0.129
* Mz 13(VLAN 26): 134.18.0.1
* Mz 14(VLAN 28): 134.18.0.65
* Mz 15(VLAN 30): 134.18.2.17
* Mz 16(VLAN 32): 134.18.2.1
 

As we were asked, all network devices must be found interconnected and have an Internet connection through a single connection.

## Limitations
For ease of work we developed the simulation of the topology in the Cisco Packet Tracer to optimally and safely design the network, knowing how many devices were going to be used, following the points that were specified.
We were asked to choose a method to perform the addressing of the network that we have selected, we chose the router on a stick method; which is one of the most common methods for inter-VLAN routing. It consists of creating sub interfaces in a single physical interface of a router; With this configuration we have the possibility of using a single interface to route the packets of several VLANs that travel through the trunk port of a switch connected to that interface.

## Contributors
- Juan Sotomayor
- Hugo Gavilánez
- Roberth Franco
- Ricardo Ramírez

## How to Execute the Project
Install [Cisco Packet Tracer](https://www.netacad.com/courses/packet-tracer) and download the project [Villa Club Etapa Estelar.pkt](../master/Villa%20Club%20Etapa%20Estelar.pkt), execute it and see the proper functioning.
